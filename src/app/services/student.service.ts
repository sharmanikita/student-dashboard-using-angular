import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) {
  }

  loadStudents() {
    return this.http.get('http://localhost:8088/api/Students');
  }

  loadStudentDetails(id) {
    return this.http.get(`http://localhost:8088/api/Students/${id}`);
  }

  loadNationalities() {
    return this.http.get('http://localhost:8088/api/Nationalities');
  }

  loadStudentFamilyDetails(id) {
    return this.http.get(`http://localhost:8088/api/Students/${id}/FamilyMembers/`);
  }

  addStudent(data) {
    return this.http.post(`http://localhost:8088/api/Students`, data);
  }

  addFamily(id, data) {
    return this.http.post(`http://localhost:8088/api/Students/${id}/FamilyMembers/`, data);
  }

  getStudentNationality(id) {
    return this.http.get(`http://localhost:8088/api/Students/${id}/Nationality/`);
  }

  getFamilyNationality(id) {
    return this.http.get(`http://localhost:8088/api/FamilyMembers/${id}/Nationality/`);
  }

  deleteStudent(familyId, nationalityId) {
    return this.http.delete(`http://localhost:8088/api/FamilyMembers/${familyId}/Nationality/${nationalityId}`);
  }

  updateStudent(id, student) {
    return this.http.put(`http://localhost:8088/api/Students/${id}`, {student});
  }

  updateStudentNationality(id, nationality) {
    return this.http.put(`http://localhost:8088/api/Students/${id}/Nationality/${nationality}`, {});
  }

  updateFamily(id, family) {
    return this.http.put(`http://localhost:8088/api/FamilyMembers/${id}`, family);
  }

  updateFamilyNationality(id, nationality) {
    return this.http.put(`http://localhost:8088/api/FamilyMembers/${id}/Nationality/${nationality}`, {});
  }

  deleteFamilyMember(id) {
    return this.http.delete(`http://localhost:8088/api/FamilyMembers/${id}`);
  }
}
