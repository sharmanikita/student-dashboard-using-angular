import {Injectable} from '@angular/core';
import {UserRoles} from '../enums/user.roles';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userRole = UserRoles.ADMIN;
  public roleState: Subject<any> = new Subject();
  public role$ = this.roleState.asObservable();

  constructor() {
  }

  changeUserRole(role) {
    this.userRole = role;
    this.roleState.next(this.userRole);
  }
}
