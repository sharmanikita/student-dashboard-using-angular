import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {StudentFormComponent} from './components/student-form/student-form.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent,
}, {
  path: 'add',
  component: StudentFormComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
