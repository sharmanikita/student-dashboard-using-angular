import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {StudentService} from '../../services/student.service';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  registerForm: FormGroup;
  nationalities: any = [];
  userRole: any;
  @Input()
  id: any;

  /**
   * Constructor
   * @param {FormBuilder} formBuilder
   * @param {Router} router
   * @param {StudentService} studentService
   * @param {AuthService} authService
   * @param {ToastrService} toastrService
   */
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private studentService: StudentService,
              private authService: AuthService,
              private toastrService: ToastrService) {
  }

  /**
   * Init
   */
  ngOnInit() {

    // set current user role
    this.userRole = this.authService.userRole;
    this.authService.role$.subscribe(role => {
      this.userRole = role;
    });

    // Load all nationalities
    this.studentService.loadNationalities().subscribe(data => {
      this.nationalities = data;
    });

    // create form
    this.registerForm = this.formBuilder.group({
      id: [null],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      nationality: [''],
      families: this.formBuilder.array([])
    });

    // check if edit, load details
    if (this.id) {
      this.loadStudentDetails(this.id);
    }
  }

  /**
   * Get form controls
   * @return {{[p: string]: AbstractControl}}
   */
  get f() {
    return this.registerForm.controls;
  }

  /**
   * Load student details if edit
   * @param id
   */
  loadStudentDetails(id) {
    this.studentService.loadStudentDetails(id).subscribe(studentData => {
      this.registerForm.controls.id.setValue(studentData['ID']);
      this.registerForm.controls.firstName.setValue(studentData['firstName']);
      this.registerForm.controls.lastName.setValue(studentData['lastName']);
      this.registerForm.controls.dateOfBirth.setValue(studentData['dateOfBirth']);
      this.studentService.getStudentNationality(studentData['ID']).subscribe(data => {
        this.registerForm.controls.nationality.setValue(data['nationality']['ID']);
      });
      this.studentService.loadStudentFamilyDetails(id).subscribe((data: any) => {
        data.forEach(family => {
          this.studentService.getFamilyNationality(family['ID']).subscribe(nData => {
            const control = this.registerForm.controls.families as FormArray;
            control.push(
              this.formBuilder.group({
                id: [family['ID']],
                firstName: [family['firstName'], Validators.required],
                lastName: [family['lastName'], Validators.required],
                dateOfBirth: [family['dateOfBirth'], Validators.required],
                relationship: [family['relationship'], Validators.required],
                nationality: [nData ? nData['ID'] : '', Validators.required]
              })
            );
          });
        });
      });
    });
  }


  /**
   * Add Family member
   */
  addFamilyMemberForm() {
    const control = this.registerForm.controls.families as FormArray;
    control.push(
      this.formBuilder.group({
        id: [null],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        dateOfBirth: ['', Validators.required],
        relationship: ['', Validators.required],
        nationality: ['', Validators.required]
      })
    );
  }

  /**
   * Remove family member
   * @param index
   * @param id
   */
  removeFamilyMemberForm(index, id) {
    const control = this.registerForm.controls.families as FormArray;
    control.removeAt(index);
    if (id) {
      this.studentService.deleteFamilyMember(id).subscribe(response => {
        this.toastrService.success('family member deleted successfully', 'Success');
      });
    }
  }

  /**
   * Add student nationality
   * @param studentId
   */
  addStudentNationality(studentId) {
    this.studentService.updateStudentNationality(studentId, this.registerForm.value.nationality).subscribe(response => {
      if (response['ID']) {
        this.toastrService.success('Nationality updated successfully', 'Success');
      } else {
        this.toastrService.error('Failed to update Nationality. Please try again', 'Failure');
      }
    });
  }

  /**
   * Add family member nationality
   * @param studentId
   * @param nationality
   */
  addFamilyMemberNationality(studentId, nationality) {
    this.studentService.updateFamilyNationality(studentId, nationality).subscribe((response: any) => {
      if (response.length > 0) {
        this.toastrService.success('Nationality updated successfully', 'Success');
      } else {
        this.toastrService.error('Failed to update Nationality. Please try again', 'Failure');
      }
    });
  }

  /**
   * Add family member
   * @param studentId
   * @param family
   */
  addFamilyMember(studentId, family) {
    this.studentService.addFamily(studentId, family).subscribe(response => {
      if (response['ID']) {
        this.toastrService.success('Family member updated successfully', 'Success');
      } else {
        this.toastrService.error('Failed to update family member. Please try again', 'Failure');
      }
    });
  }

  /**
   * Update student
   */
  updateStudent() {
    this.studentService.updateStudent(this.id, this.registerForm.value).subscribe(response => {
      if (response['ID']) {
        this.toastrService.success('Student updated successfully', 'Success');
      } else {
        this.toastrService.error('Failed to update student. Please try again', 'Failure');
      }
    });
  }

  /**
   * Update family members
   */
  updateFamilyMembers() {
    if (this.registerForm.value.families.length > 0) {
      this.registerForm.value.families.forEach(family => {
        if (family.id && family.id !== '') {
          this.studentService.updateFamily(family.id, family).subscribe(response => {
            if (response['ID']) {
              this.toastrService.success('Family member updated successfully', 'Success');
            } else {
              this.toastrService.error('Failed to update family member. Please try again', 'Failure');
            }
          });
        } else {
          this.addFamilyMember(this.id, family);
        }
      });
    }
  }


  /**
   * On submit form
   */
  onSubmit() {
    const student = {
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      dateOfBirth: this.registerForm.value.dateOfBirth
    };

    this.studentService.addStudent(student).subscribe(response => {
      if (response['ID']) {
        if (this.registerForm.value.nationality && this.registerForm.value.nationality !== '') {
          this.addStudentNationality(response['ID']);
        }
        if (this.registerForm.value.families.length > 0) {
          this.registerForm.value.families.forEach(family => {
            this.addFamilyMember(response['ID'], family);
          });
        }
        this.toastrService.success('Student Added successfully', 'Success');
      } else {
        this.toastrService.error('Failed to add student. Please try again', 'Failure');
      }
    });
  }

}
