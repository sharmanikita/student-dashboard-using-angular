import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../services/auth.service';
import {UserRoles} from '../../enums/user.roles';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  roles = UserRoles;
  role = UserRoles.ADMIN;

  /**
   * Constructor
   * @param {AuthService} authService
   * @param {ToastrService} toastrService
   */
  constructor(private authService: AuthService,
              private toastrService: ToastrService) {
  }

  /**
   * On init
   */
  ngOnInit() {
  }

  /**
   * On change user role
   * @param role
   */
  changeRole(role) {
    this.authService.changeUserRole(role);
    this.role = role;
    this.toastrService.success(`Role changed to ${role}`, `Success`);
  }


}
