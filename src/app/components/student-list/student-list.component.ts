import {Component, OnInit, ViewChild} from '@angular/core';
import {StudentService} from '../../services/student.service';
import {StudentDetailsComponent} from '../student-details/student-details.component';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  studentList: any;
  filterData: any;
  @ViewChild('studentModal') modal: StudentDetailsComponent;

  /**
   * Constructor
   * @param {StudentService} studentService
   */
  constructor(private studentService: StudentService) { }

  /**
   * On init
   */
  ngOnInit() {
    this.loadStudents();
  }

  /**
   * Load all students
   */
  loadStudents() {
    this.studentService.loadStudents().subscribe(response => {
      this.studentList = response;
    });
  }

  /**
   * Open student details modal
   * @param id
   */
  openStudentModal(id) {
    this.modal.open(id);
  }

}
