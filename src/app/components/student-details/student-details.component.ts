import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  closeModal: any;
  id: any;
  @ViewChild('content') studentModal;

  /**
   * Constructor
   * @param {NgbModal} modal
   */
  constructor(private modal: NgbModal) { }

  /**
   * On init
   */
  ngOnInit() {}

  /**
   * Open student details modal
   * @param id
   */
  open(id) {
    this.id = id;
    this.modal.open(this.studentModal).result.then((result) => {
      this.closeModal = `Closed with: ${result}`;
    }, (reason) => {
      console.log(reason);
    });
  }
}
