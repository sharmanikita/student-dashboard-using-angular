export enum UserRoles {
  ADMIN = 'admin',
  REGISTRAR = 'registrar'
}
